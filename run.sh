sudo vcdbg set awb_mode 0
cd /home/pi/Eyecaps
export PYTHONPATH=$PYTHONPATH:/home/pi/tensorflow/models/research:/home/pi/tensorflow/models/research/slim
lxterm -e 'python3 Object_detection.py' &
lxterm -e 'python3 Hub.py' &
lxterm -e 'python3 Distance.py' &
lxterm -e 'python3 Navigation.py' &
lxterm -e 'python3 Sound.py' &
lxterm -e 'python3 Remote.py' &
lxterm -e 'python3 Tracking.py' &
