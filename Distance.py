import rpyc
import time
import sys
import signal
import libs.VL53L0X as vl0
import libs.VL53L0X1 as vl2
import VL53L1X as vl1
import Logger as lg

class Distance():     
    # init method or constructor 
    def __init__(self): 
        self.c = rpyc.connect("localhost", 18861)
        self.log = lg.Logger()

        self.tof_top = vl1.VL53L1X(i2c_bus=3, i2c_address=0x29)
        self.tof_left = vl0.VL53L0X(i2c_bus=4,i2c_address=0x29)
        self.tof_right = vl2.VL53L0X(i2c_bus=2,i2c_address=0x29)
        self.tof_left.open()
        self.tof_left.start_ranging(vl0.Vl53l0xAccuracyMode.LONG_RANGE)
        self.tof_right.open()
        self.tof_right.start_ranging(vl0.Vl53l0xAccuracyMode.LONG_RANGE)
        self.tof_top.open()
        self.tof_top.start_ranging(2)
        self.timing_left =  self.tof_left.get_timing()
        self.timing_right =  self.tof_right.get_timing()
        if  self.timing_left < 20000:
            self.timing_left = 20000
        if  self.timing_right < 20000:
            self.timing_right = 20000
        signal.signal(signal.SIGINT, self.exit_handler)

    def exit_handler(self, signal, frame):
        self.tof_top.stop_ranging()
        self.tof_left.stop_ranging()
        self.tof_left.close()
        self.tof_right.stop_ranging()
        self.tof_right.close()
        sys.exit(0)

    def connect_hub(self):
        return self.c.root.connect_hub()
    
    def init(self):
        print("Distance : wait other service")
        self.c.root.distance(bool(1))
        
        return True
        
    def destance_top_send(self,tipe):
        if tipe==1:
            self.c.root.distanceTopHalf()
        elif tipe==2:
            self.c.root.distanceTopOne()
        elif tipe==3:
            self.c.root.distanceTopTwo()
        elif tipe==4:
            self.c.root.distanceTopThree()
        #print("send top")
        

    def destance_left_send(self,tipe):
        if tipe==1:
            self.c.root.distanceLeftHalf()
        elif tipe==2:
            self.c.root.distanceLeftOne()
        
        #print("send left")


    def destance_right_send(self,tipe):
        if tipe==1:
            self.c.root.distanceRightHalf()
        elif tipe==2:
            self.c.root.distanceRightOne()
        
        #print("send right")


    def reading(self):
        self.distance_left = self.tof_left.get_distance()/10
        time.sleep(self.timing_left/1000000.00)
        self.distance_right = self.tof_right.get_distance()/10
        time.sleep(self.timing_right/1000000.00)
        self.distance_top = self.tof_top.get_distance()/10
        #print("top")

        if((self.c.root.newDataDistance()==bool(1) and self.c.root.distanceMode()==bool(1)) or self.c.root.playDistance()==bool(1)):
            #print("ok")
            #print(self.distance_top)
            #print(self.distance_left)
            #print(self.distance_right)
            self.log.save("start distance")
            if int(self.distance_top) >= 5 and int(self.distance_top) <=50:
                self.destance_top_send(1)
            elif int(self.distance_top) >= 90 and int(self.distance_top) <=120:
                self.destance_top_send(2)
            elif int(self.distance_top) >= 200 and int(self.distance_top) <=220:
                self.destance_top_send(3)
            elif int(self.distance_top) >= 300 and int(self.distance_top) <=320:
                self.destance_top_send(4)

            if int(self.distance_left) >= 0 and int(self.distance_left) <=50:
                self.destance_left_send(1)
            elif int(self.distance_left) >= 80 and int(self.distance_left) <=100:
                self.destance_left_send(2)

            if int(self.distance_right) >= 0 and int(self.distance_right) <=50:
                self.destance_right_send(1)
            elif int(self.distance_right) >= 80 and int(self.distance_right) <=100:
                self.destance_right_send(2)
            
            self.c.root.cnewDataDistance(1)
            self.c.root.setplayDistance(bool(0))

        return True

time.sleep(20)
ds = Distance()
while True:
    try:
        ds.init()
        if ds.connect_hub() == True:
            break
    except:
        print("Hub no live")

while True:
    ds.reading()
