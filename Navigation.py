import rpyc
import Database
import pyproj
import time
import math
import smbus
import Logger as lg
import libs.mpu6050 as mpu
import numpy as np

class Navigation():     
    # init method or constructor 
    def __init__(self): 
        self.c = rpyc.connect("localhost", 18861)
        self.db = Database.Database()
        self.log = lg.Logger()
        self.geodesic = pyproj.Geod(ellps='WGS84')
        self.longitude = []
        self.latitude = []
        self.index = 0
        self.mapData = []
        self.noMap =""
        self.oldTrack = 0
        self.bus = smbus.SMBus(1)
        self.addr = 0x1E
        self.pi = 3.141592653
        self.xmax = 0
        self.xmin = 0
        self.ymax = 0
        self.ymin = 0
        self.xoffset = -144
        self.yoffset = - 350
        self.mode = "up"
        self.bearing = 0

        self.bus.write_byte_data(self.addr, 0x02, 0x00)

    def converData(self):
        self.seq = np.dstack((self.longitude, self.latitude))
        return self.seq[0]

    def connect_hub(self):
        return self.c.root.connect_hub()
    
    def init(self):
        print("Navigation : wait other service")
        self.c.root.navigation(bool(1))
        
        return True
    
    def checkIndex(self):
        self.realGps = self.c.root.getGpsData()
        print(self.mapData)
        print(self.realGps)
        self.fwd_azimuth, self.back_azimuth, self.distance = self.geodesic.inv(self.realGps[1], self.realGps[0], self.mapData[0][2], self.mapData[0][3])
        self.upData = self.distance
        self.fwd_azimuth, self.back_azimuth, self.distance = self.geodesic.inv(self.realGps[1], self.realGps[0], self.mapData[len(self.mapData)-1][2], self.mapData[len(self.mapData)-1][3])
        self.downData = self.distance
        '''
        self.inits = 0
        self.set = 10000
        for x in self.mapData:
            self.fwd_azimuth, self.back_azimuth, self.distance = self.geodesic.inv(self.realGps[1], self.realGps[0], x[2], x[3])
            if (self.distance <=  self.set):
                self.set = self.distance
                self.index = self.inits
            self.inits+=1
        '''
        if self.downData > self.upData:
            self.mode = "up"
            self.index = 1
        else:
            self.mode = "down"
            self.mode = len(self.mapData)-2
        print ("index :"+str(self.index))
        print ("mode :"+str(self.mode))
        return 0

    def start(self):
        self.noMap =""
        for x in self.c.root.noMap():
            self.noMap += str(x)
        
        if self.c.root.startNavigation() == bool(1) and self.c.root.startNavigationGo() == bool(0) and self.noMap != "":
            print("init")
            self.loadMap(self.noMap)
            self.checkIndex()
    
    def loadMap(self, data):
        self.mapData = self.db.show_data_map(data)
        if len(self.mapData) == 0:
            self.c.root.notFoundMap()

    def setHeading(self):
        self.xmsb = self.bus.read_byte_data(self.addr, 0x03)
        self.xlsb = self.bus.read_byte_data(self.addr, 0x04)
        self.zmsb = self.bus.read_byte_data(self.addr, 0x05)
        self.zlsb = self.bus.read_byte_data(self.addr, 0x06)
        self.ymsb = self.bus.read_byte_data(self.addr, 0x07)
        self.ylsb = self.bus.read_byte_data(self.addr, 0x08)
        self.x = self.xlsb + (self.xmsb << 8)
        self.y = self.ylsb + (self.ymsb << 8)
        self.z = self.zlsb + (self.zmsb << 8)
        time.sleep(.250)

        if self.x > 32767:
            self.x = -((65535 - self.x) + 1)
        if self.y > 32767:
            self.y = -((65535 - self.y) + 1)
        if self.z > 32767:
            self.z = -((65535 - self.z) + 1)
        self.x = self.x - self.xoffset
        self.y = self.y - self.yoffset

        self.heading = math.atan2(self.y, self.x)

        if self.heading < 0:
            self.heading += 2 * math.pi
        
        self.heading = math.degrees(self.heading)
        if self.heading  <= 90 and self.heading >= 0:
            self.bearing  = self.heading
        elif self.heading > 90 and self.heading <= 180:  
            self.bearing = self.heading
        elif self.heading > 180 and self.heading <= 270:  
            self.bearing = (self.heading - 360)
        elif self.heading > 270 and self.heading <=360:
            self.bearing = (self.heading - 360)

        return self.bearing

    def pid_controller(self):
        print("azimuth"+str(self.fwd_azimuth))

        try:
            self.v = 0
            for x in range(2):
                self.v += self.setHeading()
            self.v /= 2
        except:
            print("gagal compas")
        print("real"+str(self.v))
        self.control = (self.fwd_azimuth) - (self.v)
        if (self.control > 180):
            self.control = (self.control-360) * -1
        elif (self.control < -180):
            self.control = (self.control+360) * -1
        print("control"+ str(self.control))

        if (self.control <= 35 and self.control >= 0) or (self.control < 0 and self.control>=-35):
            self.c.root.headingUP()
            self.log.save("headingUP")
        elif (self.control < -35 and self.control > -70):
            self.c.root.headingSlantLeft()
            self.log.save("headingSlantLeft")
        elif (self.control > 35 and self.control < 70):
            self.c.root.headingSlantRight()
            self.log.save("headingSlantRight")
        elif (self.control <= -70 and self.control > -110):
            self.c.root.headingLeft()
            self.log.save("headingLeft")
        elif (self.control >= 70 and self.control < 110):
            self.c.root.headingRight()
            self.log.save("headingRight")
        elif (self.control <= 180 and self.control >= 110) or (self.control <= -110 and self.control>=-180):
            self.c.root.headingBackward()
            self.log.save("headingBackward")

        
    def reading(self):
        if self.c.root.startNavigationGo() == bool(1) and self.c.root.startNavigation() == bool(1) and self.c.root.newDataNavigation()==bool(1):
            self.log.save("start navigation")

            self.realGps = self.c.root.getGpsData()
            #self.log.tracking(self.noMap+"-"+self.realGps[1]+","+self.realGps[0])
            self.longitude.append(self.realGps[1])
            self.latitude.append(self.realGps[0])
            try:
                #print(self.mapData)
                #print(self.realGps)
                print(self.mapData[self.index][2])
                print(len(self.mapData))
                self.fwd_azimuth, self.back_azimuth, self.distance = self.geodesic.inv(self.realGps[1], self.realGps[0], self.mapData[self.index][2], self.mapData[self.index][3])
                #print("radius"+str(self.fwd_azimuth))
                        
                self.pid_controller()

                if self.mode == "up":
                    self.fwd_azimuth_new, self.back_azimuth_new, self.distance_new = self.geodesic.inv(self.realGps[1], self.realGps[0], self.mapData[self.index+6][2], self.mapData[self.index+6][3])
                    if self.distance >= self.distance_new:
                        self.index +=6
                        print("index change :"+str(self.index))

                else:
                    self.fwd_azimuth_new, self.back_azimuth_new, self.distance_new = self.geodesic.inv(self.realGps[1], self.realGps[0], self.mapData[self.index-6][2], self.mapData[self.index-6][3])
                    if self.distance >= self.distance_new:
                        self.index -=6
                        print("index change :"+str(self.index))


                if ((self.mapData[self.index][2]-(0.000018)) <= self.realGps[1] and (self.mapData[self.index][2]+(0.000018)) >= self.realGps[1]) or ((self.mapData[self.index][3]-(0.000018)) <= self.realGps[0] and (self.mapData[self.index][3]+(0.000018)) >= self.realGps[0]):
                    if self.mode == "up":
                        self.index +=5
                    else:
                        self.index -=5
                
                print("index :"+str(self.index))

                if abs(self.distance-self.oldTrack) >= 2:
                    if(self.distance<=9 and self.distance > 0):
                        self.c.root.setDistance(int(self.distance))
                    else:
                        self.c.root.setDistance(10)
                    self.oldTrack = self.distance
            except:
                print("index over")
        elif ((self.index >= len(self.mapData) and self.mode == "up") or (self.index <= 0 and self.mode == "down")) and self.c.root.startNavigationGo() == bool(1):
            self.db.insert_log_nav(self.noMap,self.converData())
            self.c.root.cnewDataNavigation(1)
            self.c.root.finish()
            self.longitude.clear()
            self.latitude.clear()
        elif (len(self.longitude)>0 and self.c.root.startNavigationGo() == bool(0)):
            self.db.insert_log_nav(self.noMap,self.converData())
            self.c.root.cnewDataNavigation(1)
            self.longitude.clear()
            self.latitude.clear()
        return True

ins = mpu.mpu6050(0x68,bus=1)
print(ins.get_temp())
accel_data = ins.get_accel_data()
print(accel_data['x'])
print(accel_data['y'])
print(accel_data['z'])
gyro_data = ins.get_gyro_data()
print(gyro_data['x'])
print(gyro_data['y'])
print(gyro_data['z'])

time.sleep(20)
nv = Navigation()
while True:
    try:
        nv.init()
        if nv.connect_hub() == True:
            break
    except:
        print("Hub no live")

while True:
    if nv.c.root.startNavigation() == bool(1) and nv.c.root.startNavigationGo() == bool(0):
        nv.start()
    nv.reading()
