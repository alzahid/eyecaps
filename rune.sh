#!/bin/sh
sudo vcdbg set awb_mode 0
cd /home/pi/Eyecaps
export PYTHONPATH=$PYTHONPATH:/home/pi/tensorflow/models/research:/home/pi/tensorflow/models/research/slim
if test $(ps -ef | grep "Object" | grep -v "grep" | wc -l) = "0"
then
    lxterm -e 'python3 Object_detection.py' &
fi
if test $(ps -ef | grep "Hub" | grep -v "grep" | wc -l) = "0"
then
    lxterm -e 'python3 Hub.py' &
fi
if test $(ps -ef | grep "Distance" | grep -v "grep" | wc -l) = "0"
then
    lxterm -e 'python3 Distance.py' &
fi
if test $(ps -ef | grep "Navigation" | grep -v "grep" | wc -l) = "0"
then
    lxterm -e 'python3 Navigation.py' &
fi
if test $(ps -ef | grep "Sound" | grep -v "grep" | wc -l) = "0"
then
    lxterm -e 'python3 Sound.py' &
fi
if test $(ps -ef | grep "Remote" | grep -v "grep" | wc -l) = "0"
then
    lxterm -e 'python3 Remote.py' &
fi
if test $(ps -ef | grep "Tracking" | grep -v "grep" | wc -l) = 0
then
    lxterm -e 'python3 Tracking.py' &
fi
exit 0
