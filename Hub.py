import rpyc
import Database
import Logger as lg
from rpyc.utils.server import ThreadedServer 

class HubData(object):
    def __init__(self):
        self.distanceMode = bool(0)
        self.objectDetectionMode = bool(0)
        self.newDataDistance = bool(1)
        self.newDataNavigation = bool(1)
        self.newDataObject = bool(1)
        self.startTracking = bool(0)
        self.startNavigation = bool(0)
        self.startTrackingGo = bool(0)
        self.startNavigationGo = bool(0)
        self.playSound = bool(0)
        self.playDistance = bool(0)
        self.playObject = bool(0)
        self.no_map = []
        self.key_remote = ""
        self.soundData = []
        self.soundDataNavigation = []
        self.objectData = []
        self.gpsData = []
        #self.loadDataMap = []
        #self.navigation = int(0)
        self.tracking = bool(0)
        self.sound = bool(0)
        self.navigation = bool(0)
        self.distance = bool(0)
        self.remote = bool(0)
        self.object = bool(0)
        self.saveTracking = bool(0)
    
    def get_saveTracking(self):
        return self.saveTracking
    
    def set_saveTracking(self, data):
        self.saveTracking = data
    
    def loadMap(self):
        return True

    def get_gpsData(self):
        return self.gpsData

    def set_gpsData(self, data):
        self.gps="Latitude=" +str(data[0]) + " and Longitude=" +str(data[1])
        #print(self.gps)
        self.gpsData = data

    def get_distanceMode(self):
        return self.distanceMode

    def get_objectDetectionMode(self):
        return self.objectDetectionMode

    def get_newDataDistance(self):
        return self.newDataDistance
    
    def get_newDataNavigation(self):
        return self.newDataNavigation

    def get_newDataObject(self):
        return self.newDataObject

    def get_startTracking(self):
        return self.startTracking

    def get_startNavigation(self):
        return self.startNavigation

    def get_startTrackingGo(self):
        return self.startTrackingGo
        
    def get_startNavigationGo(self):
        return self.startNavigationGo

    def get_playSound(self):
        return self.playSound

    def get_playDistance(self):
        return self.playDistance

    def get_playObject(self):
        return self.playObject

    def get_no_map(self):
        return self.no_map

    def get_key_remote(self):
        return self.key_remote
    
    def get_soundData(self):
        return self.soundData

    def get_soundDataNavigation(self):
        return self.soundDataNavigation

    def get_objectData(self):
        return self.objectData

    def remove_objectData(self):
        self.objectData = []

    def get_tracking(self):
        return self.tracking

    def get_sound(self):
        return self.sound

    def get_navigation(self):
        return self.navigation

    def get_distance(self):
        return self.distance

    def get_remote(self):
        return self.remote

    def get_object(self):
        return self.object

    def set_distanceMode(self, data):
        self.distanceMode = data

    def set_objectDetectionMode(self, data):
        self.objectDetectionMode = data

    def set_newDataDistance(self, data):
        self.newDataDistance = data
    
    def set_newDataObject(self, data):
        self.newDataObject = data

    def set_newDataNavigation(self, data):
        self.newDataNavigation = data

    def set_startTracking(self, data):
        self.startTracking = data

    def set_startNavigation(self, data):
        self.startNavigation = data

    def set_startTrackingGo(self, data):
        self.startTrackingGo = data

    def set_startNavigationGo(self, data):
        self.startNavigationGo = data

    def set_playSound(self, data):
        self.playSound = data

    def set_playDistance(self, data):
        self.playDistance = data

    def set_playObject(self, data):
        self.playObject = data

    def set_no_map(self, data):
        self.switcher = { 
        "KEY_1": 1,
        "KEY_2": 2,
        "KEY_3": 3,
        "KEY_4": 4,
        "KEY_5": 5,
        "KEY_6": 6,
        "KEY_7": 7,
        "KEY_8": 8,
        "KEY_9": 9
        }
        self.resultData = str(self.switcher.get(data, "space"))
        self.set_soundDataNavigation(self.resultData)
        self.no_map.append(self.resultData)

    def set_key_remote(self, data):
        self.key_remote = data

    def set_soundData(self, data):
        self.soundData.append(data)

    def set_soundDataNavigation(self, data):
        self.soundDataNavigation.append(data)

    def set_objectData(self, data):
        self.objectData = data

    def set_tracking(self, data):
        self.tracking = data

    def set_sound(self, data):
        self.sound = data

    def set_navigation(self, data):
        self.navigation = data

    def set_distance(self, data):
        self.distance = data

    def set_remote(self, data):
        self.remote = data

    def set_object(self, data):
        self.object = data
    
    def clear_no_map(self):
        self.no_map.clear()

    def clear_soundData(self):
        self.soundData.clear()
    
    def clear_soundDataNavigation(self):
        self.soundDataNavigation.clear()

    def clear_objectData(self):
        self.objectData.clear()
    
    def clear_noMap(self):
        self.no_map.clear()

    def running(self):
        print("Hub running")


class HubService(rpyc.Service):
    def __init__(self):
        self.log = lg.Logger()
        self.switcher = { 
        1: "orang",
        2: "sepeda",
        3: "mobil",
        4: "sepeda_motor",
        5: "pesawat_terbang",
        6: "bis",
        7: "kereta",
        8: "truk",
        9: "perahu",
        10: "lampu_lalulintas",
        11: "keran_kebakaran",
        13: "tanda_berhenti",
        14: "meteran_parkir",
        15: "bangku",
        16: "burung",
        17: "kucing",
        18: "anjing",
        19: "kuda",
        20: "domba",
        21: "lembu",
        22: "gajah",
        23: "beruang",
        24: "zebra",
        25: "jerapah",
        27: "ransel",
        28: "payung",
        31: "tas_tangan",
        32: "dasi",
        33: "koper",
        34: "frisbee",
        35: "ski",
        36: "papan_seluncur",
        37: "bola",
        38: "layang_layang",
        39: "tongkat_baseball",
        40: "sarung_baseball",
        41: "skateboard",
        42: "papan_luncur",
        43: "raket_tenis",
        44: "botol",
        46: "gelas_anggur",
        47: "cangkir",
        48: "garpu",
        49: "pisau",
        50: "sendok",
        51: "mangkuk",
        52: "pisang",
        53: "apel",
        54: "sandwich",
        55: "jeruk",
        56: "brokoli",
        57: "wortel",
        58: "hot_dog",
        59: "pizza",
        60: "donat",
        61: "kue",
        62: "kursi",
        63: "sofa",
        64: "tanaman_pot",
        65: "tempat_tidur",
        67: "meja_makan",
        70: "toilet",
        72: "televisi",
        73: "laptop",
        74: "mouse",
        75: "remote",
        76: "keyboard",
        77: "telepon_selular",
        78: "microwave",
        79: "oven",
        80: "pemanggang_roti",
        81: "wastafel",
        82: "kulkas",
        84: "buku",
        85: "jam",
        86: "vas",
        87: "gunting",
        88: "boneka_beruang",
        89: "pengering_rambut",
        90: "sikat_gigi"
        }
        self.switcher_number = { 
        "KEY_1": True,
        "KEY_2": True,
        "KEY_3": True,
        "KEY_4": True,
        "KEY_5": True,
        "KEY_6": True,
        "KEY_7": True,
        "KEY_8": True,
        "KEY_9": True
        }

    def exposed_soundHubNav(self):
        return dt.get_soundDataNavigation()

    def exposed_soundHub(self):
        if (len(dt.get_objectData()) >= int(1) and dt.get_objectDetectionMode() == bool(1) and dt.get_distanceMode() == bool(0)) or (len(dt.get_objectData()) >= int(1) and dt.get_objectDetectionMode() == bool(1) and dt.get_newDataDistance() == bool(0)) or (len(dt.get_objectData()) >= int(1) and dt.get_playObject()):
            dt.set_soundData("ada")
            print (dt.get_objectData())
            for x in dt.get_objectData():
                dt.set_soundData(self.convertObject(x))
            dt.set_soundData("di_depan")
            dt.set_soundData("space")
            dt.set_newDataObject(bool(1))
            
            #self.log.save("start Object hub" + " ".join([str(elem) for elem in dt.get_objectData()]))
            dt.set_playObject(bool(0))

        #print(dt.get_soundData())
        return dt.get_soundData()

    def exposed_finish(self):
        dt.set_startNavigation(bool(0))            
        dt.set_startNavigationGo(bool(0))    
        dt.set_soundDataNavigation("sudah_sampai")


    def exposed_setDistance(self,data):
        dt.set_soundDataNavigation(str(data))
        dt.set_soundDataNavigation("meter_lagi")
        #self.log.save("Distance navigation")
    
    def exposed_headingUP(self):
        dt.set_soundDataNavigation("lurus")
    
    def exposed_headingSlantLeft(self):
        dt.set_soundDataNavigation("serong_kiri")

    def exposed_headingSlantRight(self):
        dt.set_soundDataNavigation("serong_kanan")

    def exposed_headingLeft(self):
        dt.set_soundDataNavigation("belok_kiri")

    def exposed_headingRight(self):
        dt.set_soundDataNavigation("belok_kanan")

    def exposed_headingBackward(self):
        dt.set_soundDataNavigation("putar_balik")

    def exposed_gpsData(self,data):
        self.log.warning(str(data[0])+","+str(data[1]))
        dt.set_gpsData(data)

    def exposed_getGpsData(self):
        return dt.get_gpsData()
    
    def exposed_getnewDataObject(self):
        return dt.get_newDataObject()

    def exposed_setnewDataObject(self, data):
        dt.set_newDataObject(data)

    def exposed_removeSoundHub(self):
        dt.set_newDataObject(bool(1))
        dt.clear_soundData()
        
    def exposed_removeSoundHubNav(self):
        dt.clear_soundDataNavigation()
    
    def exposed_noMap(self):
        return dt.get_no_map()

    def exposed_objectDetection(self, data):
        dt.set_objectData(data)
        #print(dt.get_objectData())

    def exposed_keyRemote(self, data):
        dt.set_key_remote(data)
        #print(data)
        self.remotePress(data)

    def exposed_remote(self, data):
        dt.set_remote(data)
    
    def exposed_sound(self, data):
        dt.set_sound(data)
    
    def exposed_object(self, data):
        dt.set_object(data)

    def exposed_navigation(self, data):
        dt.set_navigation(data)

    def exposed_tracking(self, data):
        dt.set_tracking(data)
        
    def exposed_distance(self, data):
        dt.set_distance(data)

    def exposed_distanceRightOne(self):
        dt.set_soundData("penghalang_di_kanan")
        dt.set_soundData("1")
        dt.set_soundData("meter")        
        dt.set_soundData("space")
        #self.log.save("start 1 meter Kanan")

    def exposed_distanceRightHalf(self):
        dt.set_soundData("berhenti")
        dt.set_soundData("penghalang_di_kanan")
        dt.set_soundData("setengah")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 1/2 meter Kanan")

    def exposed_distanceLeftHalf(self):
        dt.set_soundData("berhenti")
        dt.set_soundData("penghalang_di_kiri")
        dt.set_soundData("setengah")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 1/2 meter Kiri")

    def exposed_distanceLeftOne(self):
        dt.set_soundData("penghalang_di_kiri")
        dt.set_soundData("1")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 1 meter Kiri")

    def exposed_distanceTopHalf(self):
        dt.set_soundData("berhenti")
        if len(dt.get_objectData()) >=1:
            dt.set_soundData("ada")
            dt.set_soundData(self.convertObject(dt.get_objectData()[0]))
            dt.set_soundData("berjarak")
        else:
            dt.set_soundData("penghalang_di_depan")
        dt.set_soundData("setengah")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 1/2 meter Depan")


    def exposed_distanceTopOne(self):
        if len(dt.get_objectData()) >=1:
            dt.set_soundData("ada")
            dt.set_soundData(self.convertObject(dt.get_objectData()[0]))
            dt.set_soundData("berjarak")
        else:
            dt.set_soundData("penghalang_di_depan")
        dt.set_soundData("1")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 1 meter Depan")


    def exposed_distanceTopTwo(self):
        if len(dt.get_objectData()) >=1:
            dt.set_soundData("ada")
            dt.set_soundData(self.convertObject(dt.get_objectData()[0]))
            dt.set_soundData("berjarak")
        else:
            dt.set_soundData("penghalang_di_depan")
        dt.set_soundData("2")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 2 meter Depan")


    def exposed_distanceTopThree(self):
        if len(dt.get_objectData()) >=1:
            dt.set_soundData("ada")
            dt.set_soundData(self.convertObject(dt.get_objectData()[0]))
            dt.set_soundData("berjarak")
        else:
            dt.set_soundData("penghalang_di_depan")
        dt.set_soundData("3")
        dt.set_soundData("meter")
        dt.set_soundData("space")
        #self.log.save("start 3 meter Depan")

    def exposed_connect_hub(self):
        if dt.get_navigation() == True and dt.get_sound() == True and dt.get_remote() == True and dt.get_object() == True and dt.get_tracking() == True and dt.get_distance() == True:
            return True
        else:
            return False
    def numberKey(self, argument):
        return self.switcher_number.get(argument, False)

    def remotePress(self, key):
        if (dt.get_startTracking() == bool(1) or dt.get_startNavigation() == bool(1)) and self.numberKey(key):
            #self.log.save("remote nomor map")
            dt.set_no_map(key)

        elif key=="KEY_CHANNELUP":
            if dt.get_startTracking() == bool(1):
                #self.log.save("remote start pelacakan")
                dt.set_soundDataNavigation("keluar_dari_pelacakan")
                dt.set_startTracking(bool(0))
            else:
                #self.log.save("remote stop pelacakan")
                dt.clear_noMap()
                dt.set_soundDataNavigation("pelacakan")
                dt.set_soundDataNavigation("masukan_nomor_map")
                dt.set_startTracking(bool(1))


        elif key=="KEY_CHANNELDOWN":
            if  dt.get_startNavigation() == bool(1):
                #self.log.save("remote start navigasi")
                dt.set_soundDataNavigation("keluar_dari_navigasi")
                dt.set_startNavigation(bool(0))
            else:
                #self.log.save("remote stop navigasi")
                dt.clear_noMap()
                dt.set_soundDataNavigation("navigasi")
                dt.set_soundDataNavigation("masukan_nomor_map")
                dt.set_startNavigation(bool(1))

        elif key=="KEY_LEFT":
            if dt.get_distanceMode() == bool(1):
                #self.log.save("remote otomasi jarak off")
                dt.set_soundDataNavigation("mematikan_deteksi_jarak")
                dt.set_distanceMode(bool(0))
            else:
                #self.log.save("remote otomasi jarak on")
                dt.set_soundDataNavigation("deteksi_jarak")
                dt.set_distanceMode(bool(1))

        elif key=="KEY_RIGHT":
            if dt.get_objectDetectionMode() == bool(1):
                #self.log.save("remote otomasi objek off")
                dt.set_newDataObject(bool(0))
                dt.set_soundDataNavigation("mematikan_deteksi_objek")
                dt.set_objectDetectionMode(bool(0))
            else:
                #self.log.save("remote otomasi objek on")
                dt.set_newDataObject(bool(1))
                dt.set_soundDataNavigation("deteksi_objek")
                dt.set_objectDetectionMode(bool(1))

        elif key=="KEY_X":
            #self.log.save("remote sekali jarak")
            dt.set_newDataObject(bool(1))
            dt.set_playDistance(bool(1))
            dt.remove_objectData()

        elif key=="KEY_Y":
            #self.log.save("remote sekali jarak")
            dt.set_playObject(bool(1))
            dt.remove_objectData()
        
        elif key=="KEY_0":
            #self.log.save("remote sekali jarak dan objek")
            dt.set_playDistance(bool(1))
            dt.set_playObject(bool(1))
            dt.remove_objectData()

        elif key=="KEY_OK":
            #self.log.save("remote ok") 
            if dt.get_startTracking() == bool(1) and len(dt.get_no_map()) >=1 and dt.get_startTrackingGo() == bool(0):
                dt.set_soundDataNavigation("memulai_pelacakan_peta")
                dt.set_startNavigation(bool(0))            
                dt.set_startNavigationGo(bool(0))            
                dt.set_startTrackingGo(bool(1))            
            elif dt.get_startTracking() == bool(1) and len(dt.get_no_map()) ==0:
                dt.set_soundDataNavigation("tidak_ada_nomor_map")

            elif dt.get_startTracking() == bool(1) and dt.get_startTrackingGo() == bool(1):
                dt.set_startTracking(bool(0))            
                dt.set_startTrackingGo(bool(0))            
                dt.set_soundDataNavigation("data_tersimpan")
                dt.set_saveTracking(bool(1))
            
            if dt.get_startNavigation() == bool(1) and len(dt.get_no_map()) >=1 and dt.get_startNavigationGo() == bool(0):
                dt.set_soundDataNavigation("memulai_navigasi_peta")
                dt.set_startNavigationGo(bool(1))
                dt.set_startTracking(bool(0))            
                dt.set_startTrackingGo(bool(0))              
            elif dt.get_startNavigation() == bool(1) and len(dt.get_no_map()) ==0:
                dt.set_soundDataNavigation("tidak_ada_nomor_map")
                
            elif dt.get_startNavigation() == bool(1) and dt.get_startNavigationGo() == bool(1):
                dt.set_startNavigation(bool(0))            
                dt.set_startNavigationGo(bool(0))  
                dt.set_soundDataNavigation("selesai")
                dt.clear_noMap()
        
        dt.set_key_remote(key)
        return True

    def exposed_setSaveTrack(self, data):
        dt.set_saveTracking(data)

    def exposed_notFoundMap(self):
        dt.set_startNavigation(bool(0))            
        dt.set_startNavigationGo(bool(0)) 
        dt.set_soundDataNavigation("data_map_tidak_ada")
        dt.set_soundDataNavigation("keluar_dari_navigasi")

    def exposed_saveTracking(self):
        return dt.get_saveTracking()

    def exposed_set_saveTracking(self, data):
        dt.set_saveTracking(data)

    def exposed_remoteKey(self):
        return dt.get_key_remote()

    def exposed_distanceConnect(self):
        return dt.get_distance()

    def exposed_changeDistance(self, mode):
        dt.set_distanceMode(bool(mode))

    def exposed_changeobjectDetection(self, mode):
        dt.set_objectDetectionMode(bool(mode))

    def exposed_startTracking(self):
        return dt.get_startTracking()
    
    def exposed_startTrackingGo(self):
        return dt.get_startTrackingGo()

    def exposed_startNavigation(self):
        return dt.get_startNavigation()
    
    def exposed_startNavigationGo(self):
        return dt.get_startNavigationGo()
    
    def exposed_playSound(self):
        return dt.get_playSound()
    
    def exposed_playDistance(self):
        return dt.get_playDistance()
    
    def exposed_setplayDistance(self, data):
        dt.set_playDistance(data)
    
    def exposed_playObject(self):
        return dt.get_playObject()
    
    def exposed_setplayObject(self, data):
        dt.set_playObject(data)
        
    def exposed_distanceMode(self):
        return dt.get_distanceMode()
    
    def exposed_newDataDistance(self):
        return dt.get_newDataDistance()

    def exposed_newDataNavigation(self):
        return dt.get_newDataNavigation()

    def exposed_objectDetectionMode(self):
        return dt.get_objectDetectionMode()
    
    def exposed_cnewDataDistance(self, key):
        if key==1:
            dt.set_newDataDistance(bool(0))
        elif key==2:
            dt.set_newDataDistance(bool(1))
    
    def exposed_cnewDataNavigation(self, key):
        if key==1:
            dt.set_newDataNavigation(bool(0))
        elif key==2:
            dt.set_newDataNavigation(bool(1))
    

    def on_connect(self, conn):
        # code that runs when a connection is created
        # (to init the service, if needed)
        pass

    def on_disconnect(self, conn):
        # code that runs after the connection has already closed
        # (to finalize the service, if needed)
        pass

    def convertObject(self, argument):
        return self.switcher.get(argument, "space")  

if __name__ == "__main__":
    dt = HubData()
    dt.running()
    t = ThreadedServer(HubService, port=18861)
    t.start()
