from lirc import RawConnection
import rpyc
import time
import sys
import signal
import Logger as lg

class Remote():     
    # init method or constructor 
    def __init__(self): 
        self.c = rpyc.connect("localhost", 18861)
        self.log = lg.Logger()
        self.conn = RawConnection()
        signal.signal(signal.SIGINT, self.exit_handler)

    def exit_handler(self, signal, frame):
        sys.exit(0)

    def connect_hub(self):
        return self.c.root.connect_hub()
    
    def init(self):
        print("Remote : wait other service")
        self.c.root.remote(bool(1))

    def running(self):
        try:
            self.keypress = self.conn.readline(.0001)
        except:
            self.keypress=""
                
        if (self.keypress != "" and self.keypress != None):
                    
            self.data = self.keypress.split()
            self.sequence = self.data[1]
            self.command = self.data[2]
            
            #ignore command repeats
            #("sis"+self.command)
            if (self.sequence != "00"):
                return True
            else:
                self.log.save("remote key "+str(self.command))
                #print(self.command)
                self.c.root.keyRemote(self.command)
                return self.command

time.sleep(20)

rmt = Remote()
print("Starting Up...")

while True:
    try:
        rmt.init()
        if rmt.connect_hub() == True:
            break
    except:
        print("Hub no live")

while True:         
    rmt.running()
