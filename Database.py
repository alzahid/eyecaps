import sqlite3 as lite
from operator import itemgetter
class Database():     
    # init method or constructor 
    def __init__(self):
        self.con = lite.connect('eyecaps.db')

    def show_data_map(self, no_map):
        cur = self.con.cursor()
        sql = 'SELECT * FROM map_step Where no_map=?'
        cur.execute(sql, (no_map,))
        rows = cur.fetchall()
        return rows

    def show_log_nav(self, no_map):
        cur = self.con.cursor()
        sql = 'SELECT * FROM log_nav Where no_map=?'
        cur.execute(sql, (no_map,))
        rows = cur.fetchall()
        return rows

    def insert_data_map(self, no_map, map_step):
        map_step = sorted(map_step.tolist(), key=itemgetter(0))
        for x in range(len(map_step)):
            map_step[x].insert(0,int(no_map))
        cur = self.con.cursor()
        sql = 'DELETE FROM map_step WHERE no_map=?'
        cur.execute(sql, (no_map,))
        cur.executemany("INSERT INTO map_step(no_map, lon, lat) VALUES(?, ?, ?)", map_step)
        self.con.commit()
        return True

    def insert_log_nav(self, no_map, map_step):
        map_step = sorted(map_step.tolist(), key=itemgetter(0))
        for x in range(len(map_step)):
            map_step[x].insert(0,int(no_map))
        cur = self.con.cursor()
        print(map_step)
        sql = 'DELETE FROM log_nav WHERE no_map=?'
        cur.execute(sql, (no_map,))
        cur.executemany("INSERT INTO log_nav(no_map, lon, lat) VALUES(?, ?, ?)", map_step)
        self.con.commit()
        return True
