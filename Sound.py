
import simpleaudio as sa
import rpyc
import Database
import time
import Logger as lg


class Sound():

    def __init__(self):
        self.c = rpyc.connect("localhost", 18861)
        self.log = lg.Logger()
        self.play_data_sound = []
        self.play_data_sound_navigation = []

    def play_single_sound(self, sound):
        wave_obj = sa.WaveObject.from_wave_file("/home/pi/Eyecaps/sound/"+ sound +".wav")
        play_obj = wave_obj.play()
        play_obj.wait_done()
        
    def connect_hub(self):

        return self.c.root.connect_hub()
        
    def play_sound(self):
        if len(self.play_data_sound) != 0:
            self.log.save("start sound "+ " ".join([str(elem) for elem in self.play_data_sound]))
    
        for sound in self.play_data_sound:
            #print(sound)

            if(sound=="space"):
                time.sleep(0.1)
            else:
                print(str(sound))
                wave_obj = sa.WaveObject.from_wave_file("/home/pi/Eyecaps/sound/"+ str(sound) +".wav")
                play_obj = wave_obj.play()
                play_obj.wait_done()

        if len(self.play_data_sound) != 0:
            self.c.root.removeSoundHub()
        self.play_data_sound = []
    
    def play_sound_navigation(self):
        if len(self.play_data_sound_navigation) != 0:
            self.log.save("start sound "+ " ".join([str(elem) for elem in self.play_data_sound_navigation]))

        for sound in self.play_data_sound_navigation:
            if(sound=="space"):
                time.sleep(0.1)
            else:
                print(str(sound))
                wave_obj = sa.WaveObject.from_wave_file("/home/pi/Eyecaps/sound/"+ str(sound) +".wav")
                play_obj = wave_obj.play()
                play_obj.wait_done()
        if len(self.play_data_sound_navigation) != 0:
            self.c.root.removeSoundHubNav()
        self.play_data_sound_navigation = []

    def get_sound_hub(self):
        self.play_data_sound = self.c.root.soundHub()

    def get_sound_hub_nav(self):
        self.play_data_sound_navigation = self.c.root.soundHubNav()

    def remove_sound_hub(self):
        self.c.root.cnewDataDistance(2)
        self.c.root.cnewDataNavigation(2)


    def init(self):
        print("Sound : wait other service")
        self.c.root.sound(bool(1))
        self.play_single_sound("mohon_tunggu")
        time.sleep(0.2)

    def reading(self):
        self.get_sound_hub_nav()        
        self.play_sound_navigation()
        self.get_sound_hub()
        self.play_sound()
        self.remove_sound_hub()
        return True

time.sleep(20)

sd = Sound()

while True:
    try:
        sd.init()
        if sd.connect_hub() == True:
            break
    except:
        print("Hub no live")

sd.play_single_sound("selamat_datang_di_eyecaps")
while True:
    sd.reading()
