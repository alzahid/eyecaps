import serial
import pynmea2

def parseGPS(stri):
	if stri.find('GGA') > 0:
		msg = pynmea2.parse(stri)
		lat=msg.lat
		lng=msg.lon
		gps="Latitude=" +str(lat) + "and Longitude=" +str(lng)
		print(gps)
        
serialPort = serial.Serial("/dev/ttyAMA0", 9600, timeout=0.5)

while True:
    stri = serialPort.readline()
    parseGPS(stri)
