from simple_pid import PID



class Navigation():     
    # init method or constructor 
    def pid_controller(self):
        self.pid = PID(1, 0.1, 0.05, setpoint=40)
        self.v = 90
        print("real"+str(self.v))
        self.control = self.pid(self.v)
        print("control"+ str(self.control))

        if (self.control <= 25 and self.control >= 0) or (self.control < 0 and self.control>=-20):
            print("headingUP")
        elif (self.control < -25 and self.control > -70):
            print("headingSlantLeft")
        elif (self.control > 25 and self.control < 70):
            print("headingSlantRight")
        elif (self.control <= -70 and self.control > -110):
            print("headingLeft")
        elif (self.control >= 70 and self.control < 110):
            print("headingRight")
        elif (self.control <= 180 and self.control >= 110) or (self.control <= -110 and self.control>=-180):
            print("headingBackward")


nv = Navigation()

while True:
    nv.pid_controller()
