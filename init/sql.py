import sqlite3 as lite

con = lite.connect('eyecaps.db')

with con:

    cur = con.cursor()

    cur.execute("DROP TABLE IF EXISTS map_step")
    cur.execute("DROP TABLE IF EXISTS log_nav")
    cur.execute("CREATE TABLE map_step(id INTEGER PRIMARY KEY, no_map INT, lon REAL, lat REAL)")
    cur.execute("CREATE TABLE log_nav(id INTEGER PRIMARY KEY, no_map INT, lon REAL, lat REAL)")
    print("Sukses.")