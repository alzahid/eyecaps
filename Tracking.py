from pykalman import KalmanFilter
import numpy as np
import visvalingamwyatt as vw
import serial
import time
import string
import pynmea2
import rpyc
import Database

type("text".encode("utf-8"))
class Tracking():     
    # init method or constructor 
    def __init__(self):
        self.c = rpyc.connect("localhost", 18861)
        self.port="/dev/ttyAMA0"
        self.longitude = []
        self.latitude = []
        self.db = Database.Database()
        
    def connect_hub(self):
        return self.c.root.connect_hub()
    
    def init(self):
        print("Tracking : wait other service")
        self.c.root.tracking(bool(1))
        
        return True
    
    def visvalingamwyattStart(self, points):
        self.simplifier = vw.Simplifier(points)
        #print(self.simplifier.simplify(threshold=0.00009))
        return self.simplifier.simplify(ratio=0.2)

    def kalmanFilterStart(self, measurements):
        measurements = np.asarray(measurements)
        #print(measurements)

        self.initial_state_mean = [measurements[0, 0],0,measurements[0, 1],0]
        self.transition_matrix = [[1, 1, 0, 0],[0, 1, 0, 0],[0, 0, 1, 1],[0, 0, 0, 1]]
        self.observation_matrix = [[1, 0, 0, 0],[0, 0, 1, 0]]

        self.kf = KalmanFilter(transition_matrices = self.transition_matrix,
                        observation_matrices = self.observation_matrix,
                        initial_state_mean = self.initial_state_mean)

        self.kf = self.kf.em(measurements, n_iter=5)
        (smoothed_state_means, smoothed_state_covariances) = self.kf.smooth(measurements)
        #print(smoothed_state_means)
        #print(smoothed_state_means[:, 0:3])
        clean = np.delete(smoothed_state_means[:, 0:3],[1,3],axis=1)
        #print(clean)
        return clean

    def reading(self):
        self.lat = 0 
        self.lng = 0
        self.indexGps = 0
        try:
            for x in range(10):
                self.dataout =pynmea2.NMEAStreamReader()
                self.ser=serial.Serial(self.port,baudrate=9600,timeout=0.5)
                self.newdata=str(self.ser.readline()).replace("b'",'').replace(r"\r\n'",'')
                if self.newdata[0:6]=="$GPRMC":
                    self.newmsg=pynmea2.parse(self.newdata)
                    if(self.newmsg.latitude!=0 or self.newmsg.longitude!=0):
                        self.lat+=self.newmsg.latitude
                        self.lng+=self.newmsg.longitude
                    else:
                        self.indexGps+=1
                else:
                    self.indexGps+=1
            self.lat /= (10 - self.indexGps)
            self.lng /= (10 - self.indexGps)
            if self.c.root.startTrackingGo() == bool(1):
                self.longitude.append(self.lng)
                self.latitude.append(self.lat)
            self.dataGps = []
            self.dataGps.append(self.lat)
            self.dataGps.append(self.lng)
            self.c.root.gpsData(self.dataGps)
            gps="Latitude=" +str(self.lat) + " and Longitude=" +str(self.lng)
            print(gps)
        except:
            print("gagal gps")
       

    def converData(self):
        self.seq = np.dstack((self.longitude, self.latitude))
        self.longitude.clear()
        self.latitude.clear()
        return self.visvalingamwyattStart(self.kalmanFilterStart(self.seq[0]))
    
    def saveDatabase(self):
        if self.c.root.saveTracking() == bool(1):
            self.c.root.setSaveTrack(bool(0))
            self.noMap =""
            for x in self.c.root.noMap():
                self.noMap += str(x)

            #print("nomor map:"+self.noMap)
            self.db.insert_data_map(self.noMap,self.converData())

            #print (self.db.show_data_map(self.noMap))


time.sleep(20)

tr = Tracking()

while True:
    try:
        tr.init()
        if tr.connect_hub() == True:
            break
    except:
        print("Hub no live")

while True:
    tr.reading()
    tr.saveDatabase()

