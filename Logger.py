import logging
import datetime

class Logger():     
    # init method or constructor 
    def __init__(self): 
        self.x = datetime.datetime.now()
        self.logger = logging.getLogger(__name__)
        self.c_handler = logging.FileHandler('log/warning'+str(self.x.day)+'-'+str(self.x.month)+'-'+str(self.x.year)+'.log')
        self.f_handler = logging.FileHandler('log/log-'+str(self.x.day)+'-'+str(self.x.month)+'-'+str(self.x.year)+'.log')
        self.c_handler.setLevel(logging.WARNING)
        self.f_handler.setLevel(logging.ERROR)
        self.c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        self.f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.c_handler.setFormatter(self.c_format)
        self.f_handler.setFormatter(self.f_format)

        # Add handlers to the logger
        self.logger.addHandler(self.c_handler)
        self.logger.addHandler(self.f_handler)

    def warning(self, text): 
        self.logger.warning(text)
        
    def save(self, text): 
        self.logger.error(text)